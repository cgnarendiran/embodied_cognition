#Implementation of the 3SG action model learning algorithm

plainEffectFormula = {}																			#set of propositional atoms representing dependencies between actions and effects
conditionalEffectFormula = {}																	#set of propositional atoms representing dependencies between actions, preconditions and effects
pcPos = {}																						#positive count for an atom
pcNeg = {}																						#negative count for an atom
minProbability = 0.95																			#the minimum probability for an atom to be considered as a usefully learned (grounded) operator
minExamples = 10																				#the minimum number of examples needed to be seen for an atom
memoryLength = 8000																				#the threshold number of steps before an atom is discarded unless it's probability is sufficiently high

def getModifiedFluents( observation1, observation2 ):											#the set of literals that are different in the two observations
	fluents = []
	for condition in observation1:
		if condition not in observation2:
			entry = '~' + str( condition )
			fluents.append( entry )
	
	for condition in observation2:
		if condition not in observation1:
			fluents.append( str( condition ) )	
	return fluents

def tsg( observation1, action, observation2, representationDict ):								#the 3SG algorithm
	global plainEffectFormula
	global conditionalEffectFormula
	global pcPos
	global pcNeg
	
	for atom in plainEffectFormula:
		plainEffectFormula[ atom ] = plainEffectFormula[ atom ] + 1								#updating memory record of atoms
	
	for atom in conditionalEffectFormula:
		conditionalEffectFormula[ atom ] = conditionalEffectFormula[ atom ] + 1					#updating memory record of atoms
	
	modifiedFluents = getModifiedFluents( observation1, observation2 )
	
	for fluent in modifiedFluents:																#generalisation step
		entry = str( action ) + ';' + str( fluent )
		if entry not in plainEffectFormula:
			plainEffectFormula[ entry ] = 1
			pcPos[ entry ] = 1
			pcNeg[ entry ] = 0
		else:
			pcPos[ entry ] = pcPos[ entry ] + 1
			for condition in observation1:
				newEntry = entry + ';' + str( condition )
				if newEntry in conditionalEffectFormula:
					pcPos[ newEntry ] = pcPos[ newEntry ] + 1
				newEntry = entry + ';~' + str( condition )
				if newEntry in conditionalEffectFormula:
					pcNeg[ newEntry ] = pcNeg[ newEntry ] + 1
	
	for	condition1 in representationDict:
		for ctr in range(0,2):
			if ctr == 1:
				condition1 = '~' + condition1

			if ( condition1[ 0 ] != '~' and condition1 not in observation2 ) or ( condition1[ 0 ] == '~' and condition1[ 1: ] in observation2 ):
				entry = str( action ) + ';' + str( condition1 )
				if entry in plainEffectFormula:
					pcNeg[ entry ] = pcNeg[ entry ] + 1
					for condition2 in representationDict:											#specification step
						if condition2 not in observation1:
							newEntry = str( action ) + ';' + str( condition1 ) + ';' + str( condition2 )
							if newEntry not in conditionalEffectFormula:
								conditionalEffectFormula[ newEntry ] = 1
								pcPos[ newEntry ] = 0
								pcNeg[ newEntry ] = 0
	
	toDelete = []
	for atom in conditionalEffectFormula:														#simplification step 1
		if conditionalEffectFormula[ atom ] > memoryLength:
			if pcPos[ atom ] == 0 and pcNeg[ atom ] == 0:
				toDelete.append( atom )
			else:
				if pcPos[ atom ]/( pcPos[ atom ] + pcNeg[ atom ] ) < minProbability:
					toDelete.append( atom )
				
	for atom in toDelete:
		conditionalEffectFormula.pop( atom )
	
	toDelete = []
	for atom in plainEffectFormula:
		if plainEffectFormula[ atom ] > memoryLength:											#simplification step 2
			if pcPos[ atom ] + pcNeg[ atom ] < minExamples:
				toDelete.append( atom )
			else:
				relevanceFlag = False
				for atom2 in conditionalEffectFormula:
					if atom in atom2:
						relevanceFlag = True
						break
				if not relevanceFlag:
					if pcPos[ atom ] == 0 and pcNeg[ atom ] == 0:
						toDelete.append( atom )
					else:
						if pcPos[ atom ]/( pcPos[ atom ] + pcNeg[ atom ] ) < minProbability:
							toDelete.append( atom )
	
	for atom in toDelete:
		plainEffectFormula.pop( atom )