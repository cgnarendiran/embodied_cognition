
"use strict";

let TableList = require('./TableList.js');
let NavigationControl = require('./NavigationControl.js');
let Wall = require('./Wall.js');
let WaypointList = require('./WaypointList.js');
let MagicButton = require('./MagicButton.js');
let WallList = require('./WallList.js');
let Waypoint = require('./Waypoint.js');
let ColumnList = require('./ColumnList.js');
let Column = require('./Column.js');
let NavigationControlStatus = require('./NavigationControlStatus.js');
let ARPairList = require('./ARPairList.js');
let ARPair = require('./ARPair.js');
let Table = require('./Table.js');
let Trajectory = require('./Trajectory.js');
let TrajectoryList = require('./TrajectoryList.js');
let DockingInteractorGoal = require('./DockingInteractorGoal.js');
let NavigateToAction = require('./NavigateToAction.js');
let LocalizeActionGoal = require('./LocalizeActionGoal.js');
let LocalizeResult = require('./LocalizeResult.js');
let LocalizeGoal = require('./LocalizeGoal.js');
let DockingInteractorActionResult = require('./DockingInteractorActionResult.js');
let LocalizeActionResult = require('./LocalizeActionResult.js');
let NavigateToResult = require('./NavigateToResult.js');
let NavigateToActionFeedback = require('./NavigateToActionFeedback.js');
let DockingInteractorResult = require('./DockingInteractorResult.js');
let DockingInteractorFeedback = require('./DockingInteractorFeedback.js');
let LocalizeActionFeedback = require('./LocalizeActionFeedback.js');
let NavigateToActionResult = require('./NavigateToActionResult.js');
let LocalizeFeedback = require('./LocalizeFeedback.js');
let DockingInteractorAction = require('./DockingInteractorAction.js');
let NavigateToActionGoal = require('./NavigateToActionGoal.js');
let LocalizeAction = require('./LocalizeAction.js');
let DockingInteractorActionGoal = require('./DockingInteractorActionGoal.js');
let DockingInteractorActionFeedback = require('./DockingInteractorActionFeedback.js');
let NavigateToFeedback = require('./NavigateToFeedback.js');
let NavigateToGoal = require('./NavigateToGoal.js');

module.exports = {
  TableList: TableList,
  NavigationControl: NavigationControl,
  Wall: Wall,
  WaypointList: WaypointList,
  MagicButton: MagicButton,
  WallList: WallList,
  Waypoint: Waypoint,
  ColumnList: ColumnList,
  Column: Column,
  NavigationControlStatus: NavigationControlStatus,
  ARPairList: ARPairList,
  ARPair: ARPair,
  Table: Table,
  Trajectory: Trajectory,
  TrajectoryList: TrajectoryList,
  DockingInteractorGoal: DockingInteractorGoal,
  NavigateToAction: NavigateToAction,
  LocalizeActionGoal: LocalizeActionGoal,
  LocalizeResult: LocalizeResult,
  LocalizeGoal: LocalizeGoal,
  DockingInteractorActionResult: DockingInteractorActionResult,
  LocalizeActionResult: LocalizeActionResult,
  NavigateToResult: NavigateToResult,
  NavigateToActionFeedback: NavigateToActionFeedback,
  DockingInteractorResult: DockingInteractorResult,
  DockingInteractorFeedback: DockingInteractorFeedback,
  LocalizeActionFeedback: LocalizeActionFeedback,
  NavigateToActionResult: NavigateToActionResult,
  LocalizeFeedback: LocalizeFeedback,
  DockingInteractorAction: DockingInteractorAction,
  NavigateToActionGoal: NavigateToActionGoal,
  LocalizeAction: LocalizeAction,
  DockingInteractorActionGoal: DockingInteractorActionGoal,
  DockingInteractorActionFeedback: DockingInteractorActionFeedback,
  NavigateToFeedback: NavigateToFeedback,
  NavigateToGoal: NavigateToGoal,
};
