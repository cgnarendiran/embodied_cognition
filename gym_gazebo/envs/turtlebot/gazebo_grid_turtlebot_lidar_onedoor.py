import gym
import rospy
import roslaunch
import time
import numpy as np

# import tf
from gym import utils, spaces
from gym_gazebo.envs import gazebo_env
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point
from geometry_msgs.msg import Quaternion
from std_srvs.srv import Empty
from gazebo_msgs.srv import GetModelState
from sensor_msgs.msg import LaserScan
from gym.utils import seeding
from gazebo_msgs.srv import DeleteModel, SpawnModel

class GazeboGridTurtlebotLidarEnv(gazebo_env.GazeboEnv):

    def __init__(self):
        # Launch the simulation with the given launchfile name
        gazebo_env.GazeboEnv.__init__(self, "GazeboGridTurtlebotLidar_v0.launch")
        self.vel_pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=5)
        self.unpause = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
        self.pause = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
        self.reset_proxy = rospy.ServiceProxy('/gazebo/reset_simulation', Empty)
        rospy.wait_for_service("/gazebo/delete_model")
        rospy.wait_for_service("/gazebo/spawn_sdf_model")
        print("Got the Gazebo services running")
        # self.delete_model = rospy.ServicePoxy("/gazebo/delete_model", DeleteModel)
        self.spawn_model = rospy.ServiceProxy("/gazebo/spawn_sdf_model", SpawnModel)

        

        #objects co-ordinates:
        self.model_cords = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
        #list of objects in the enviroment including the robot:
        self.objects_dict = {'cube1':['unit_box', 'link'], 'cube2': ['unit_box_0', 'link'], 'turtlebot':['mobile_base','']}
        self.action_space = spaces.Discrete(3) #F,TL,TR
        self.reward_range = (-np.inf, np.inf)
        self.direction = 0
        # door params
        self.up_door    =   "up_door"
        self.down_door  =   "down_door"
        self.up_door_pose  =   Pose(Point(x=4.0, y=5.0,  z=0),   Quaternion(x=0,y=0,z=0,w=1) )
        self.down_door_pose  =   Pose(Point(x=4.0, y=3.0,  z=0),   Quaternion(x=0,y=0,z=0,w=1) )
        with open("$GAZEBO_MODEL_PATH/drc_practice_door_4x8/model.sdf", "r") as f:
            self.product_xml = f.read()
        self.down_door_open = 0
        self.up_door_open = 0
        self._seed()


    def _observe(self,data,new_ranges):
        observations = []
        done = False
        self.down_door_open = 0
        self.up_door_open = 0
        
        # discretized_ranges = []
        # min_range = 0.1
        # mod = len(data.ranges)/new_ranges
        for obj in sorted(self.objects_dict.keys()):
            observations.append(round(self.model_cords(self.objects_dict[obj][0], self.objects_dict[obj][1] ).pose.position.x, 1))
            observations.append(round(self.model_cords(self.objects_dict[obj][0], self.objects_dict[obj][1] ).pose.position.y, 1))  


        pos = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.position
        if round(pos.x,1) ==4.5 and round(pos.y,1) ==3.5:
            if self.direction == 3:
                # self.spawn_model(self.item_name, self.product_xml, "", self.item_pose, "world")
                self.delete_model(self.down_door)
                self.down_door_open = 1
                # reward = 10

        # pos = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.position
        if round(pos.x,1) ==4.5 and round(pos.y,1) ==4.5:
            if self.direction == 3:
                # self.spawn_model(self.item_name, self.product_xml, "", self.item_pose, "world")
                self.delete_model(self.up_door)
                self.up_door_open = 1
                # reward = 10
        if round(pos.x,1) ==4.5 and round(pos.y,1) == 5.5:
            if self.direction == 1:
                self.delete_model(self.item_name)
                self.up_door_open = 1

                # reward = 10
        # direction of the robot:
        # global yaw
        # yaw = 0
        # direction = 0
        # ori = self.model_cords(objects_dict['turtlebot'][0],objects_dict['turtlebot'][1]).pose.orientation
        # orientation_list = [ori.x, ori.y, ori.z, ori.w]
        # # (_, _, yaw) = tf.transformations.euler_from_quaternion (orientation_list)
        # if -0.05<=round(yaw,2)<=0.05:
        #     direction = 1
        # elif 1.52<=round(yaw,2)<=1.62:
        #     direction = 0
        # elif 3.09<=round(yaw,2)<=3.19:
        #     direction = 3
        # elif 4.64<=round(yaw,2)<=4.76:
        #     direction = 2
        observations.append(self.direction) 
        #append goal cords
        observations.append(4.5)
        observations.append(4.5)
        # append door conditions:
        observations.append(self.down_door_open)
        observations.append(self.up_door_open)
        print(observations)
        return observations,done
        # laser scan observations:
        # for i, item in enumerate(data.ranges):
        #     if (i%mod==0):
        #         if data.ranges[i] == float ('Inf'):
        #             discretized_ranges.append(6)
        #         elif np.isnan(data.ranges[i]):
        #             discretized_ranges.append(0)
        #         else:
        #             discretized_ranges.append(int(data.ranges[i]))
        #     if (min_range > data.ranges[i] > 0):
        #         done = True
        # return discretized_ranges,done

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _step(self, action):

        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            self.unpause()
        except (rospy.ServiceException) as e:
            print ("/gazebo/unpause_physics service call failed")

        data = None
        while data is None:
            try:
                data = rospy.wait_for_message('/scan', LaserScan, timeout=5)
            except:
                pass


        if self.down_door_open ==1 or self.up_door_open ==1:
            action = 3
            now = rospy.get_rostime().to_sec()
            vel_cmd = Twist()
            vel_cmd.linear.x = 0.5
            vel_cmd.angular.z = 0.0
            while rospy.get_rostime().to_sec()< now+0.5:
                self.vel_pub.publish(vel_cmd)
            if self.down_door_open ==1:
                self.spawn_sdf_model(down_door)
            if self.up_door_open ==1:
                self.spawn_sdf_model(up_door)


        if action == 0: #FORWARD
            now = rospy.get_rostime().to_sec()
            vel_cmd = Twist()
            vel_cmd.linear.x = 1.0
            vel_cmd.angular.z = 0.0
            while rospy.get_rostime().to_sec()< now+0.2:
                self.vel_pub.publish(vel_cmd)

            # check for collision with wall:
            if (data.ranges[int(round(len(data.ranges)/2))]<=0.2):
                vel_cmd.linear.x = -0.5
                reward = -25
                now = rospy.get_rostime().to_sec()
                while rospy.get_rostime().to_sec()< now+0.2:
                    self.vel_pub.publish(vel_cmd)
            else:
                now = rospy.get_rostime().to_sec()
                while rospy.get_rostime().to_sec()< now+0.8:
                    self.vel_pub.publish(vel_cmd)        
        elif action == 1: #TURNLEFT
            self.direction-=1
            if self.direction==-1:
                self.direction=3
            now = rospy.get_rostime().to_sec()
            vel_cmd = Twist()
            vel_cmd.linear.x = 0.0
            vel_cmd.angular.z = 1.57
            while rospy.get_rostime().to_sec()< now+1.0:
                self.vel_pub.publish(vel_cmd)
        elif action == 2: #TURNRIGHT
            self.direction+=1
            if self.direction==4:
                self.direction=0
            now = rospy.get_rostime().to_sec()
            vel_cmd = Twist()
            vel_cmd.linear.x = 0.0
            vel_cmd.angular.z = -1.57
            while rospy.get_rostime().to_sec()< now+1.0:
                self.vel_pub.publish(vel_cmd)


        rospy.wait_for_service('/gazebo/pause_physics')
        try:
            #resp_pause = pause.call()
            self.pause()
        except (rospy.ServiceException) as e:
            print ("/gazebo/pause_physics service call failed")

        state,done = self._observe(data,5)

        # reward for reaching goal position:
        pos = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.position
        if round(pos.x,1) ==4.5 and round(pos.y,1) ==4.5:
                reward = 95
        if not done:
            if action == 0:
                reward = 5
            else:
                reward = 1
        else:
            reward = -200

        return state, reward, done, {}

    def _reset(self):

        # Resets the state of the environment and returns an initial observation.
        rospy.wait_for_service('/gazebo/reset_simulation')
        try:
            #reset_proxy.call()
            self.reset_proxy()
        except (rospy.ServiceException) as e:
            print ("/gazebo/reset_simulation service call failed")

        # Unpause simulation to make observation
        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            #resp_pause = pause.call()
            self.unpause()
        except (rospy.ServiceException) as e:
            print ("/gazebo/unpause_physics service call failed")

        try:
            #spawn the door()
            self.spawn_model(item_name, self.product_xml, "", item_pose, "world")
        except (rospy.ServiceException) as e:
            print ("/gazebo/unpause_physics service call failed")
 
        #read laser data
        data = None
        while data is None:
            try:
                data = rospy.wait_for_message('/scan', LaserScan, timeout=5)
            except:
                pass

        rospy.wait_for_service('/gazebo/pause_physics')
        try:
            #resp_pause = pause.call()
            self.pause()
        except (rospy.ServiceException) as e:
            print ("/gazebo/pause_physics service call failed")

        state = self._observe(data,5)
        return state