#implementation of the IMRL algorithm

import random
import datetime

def imrl( representationDict, initialState, goals, salientEvent, allActions, actionProbabilityThreshold, functionDict, alpha = 0.2, gamma = 0.9, epsilon = 0.1, tau = 0.5, iterations = 100000, outputFile = 'op/imrlOutput' ):
	
	startTime = datetime.datetime.now()
	numberOfActions = len( allActions )
	numberOfOptions = 2
	
	opFileName = outputFile + str(startTime).replace( ':', '-' ) + str('.txt')										#adding the time stamp to the name of the output file
	opFile = open( opFileName, 'w' )
	opFile.write( 'startTime: ' + str( startTime ) + '\nAlpha: ' + str( alpha ) + '\nGamma: ' + str ( gamma ) + '\nEpsilon: ' + str( epsilon ) + '\nTau: ' + str( tau ) + '\nNumber of actions: ' + str( numberOfActions ) + '\nNumber of options: ' + str( numberOfOptions ) + '\nIterations: ' + str( iterations ) + '\n\n' )
	
	stateValue = [ dict() for i in range( numberOfActions + numberOfOptions ) ]							#state action values; one dictionary for each action
	stateSpace = []																						#contains all unique state ids
	atGoal = False
	salientEventIndex = 0
	
	actionCounter1 = 0
	actionCounter2 = 0
	eRewardCounter = 0
	actionsPerERewardCounter = []
	
	optionInitiationSet = []
	optionValueFunction = []
	optionTerminationStateSet = []
	optionModel = [ dict() for i in range( numberOfOptions ) ]
	optionCounter = 0
	optionEventObserved = []
	addedOptionIndex = -1																				#used to identify a newly added option to skip in block 2 of imrl code
	
	for ctr1 in range( numberOfOptions ):
		optionInitiationSet.append( [] )
		optionValueFunction.append( [ dict() for i in range( numberOfActions + numberOfOptions ) ] )
		optionTerminationStateSet.append( [] )
		optionEventObserved.append( 0 )

	currentState = list( initialState )	
	currentStateID = functionDict[ 'getStateID' ]( representationDict, currentState )						#get the initial state ID
		
	if currentStateID not in stateSpace:																	#create space to represent state if not seen before
		for ctr2 in range( numberOfOptions ):
			optionModel[ ctr2 ][ int( str( currentStateID ) + str( currentStateID ) ) ] = 0
			optionModel[ ctr2 ][ currentStateID ] = 0
			for ctr3 in range( len( stateSpace ) ):
				optionModel[ ctr2 ][ int( str( stateSpace[ ctr3 ] ) + str( currentStateID ) ) ] = 0
				optionModel[ ctr2 ][ int( str( currentStateID ) + str( stateSpace[ ctr3 ] ) ) ] = 0
					
		stateSpace.append( currentStateID )
			
		for ctr2 in range( numberOfActions + numberOfOptions ):
			stateValue[ ctr2 ][ currentStateID ] = 0													#initialisation of behaviour value function
			for ctr3 in range( numberOfOptions ):
				optionValueFunction[ ctr3 ][ ctr2 ][ currentStateID ] = 0								#initialisation of option value function
					
	
	eReward = 0																							#initial extrinsic reward
	iReward = 0																							#initial intrinsic reward
	
	for ctrn in range( iterations ):
			
		if random.random() > epsilon:																	#exploit
			values = []
			maxActions = []
			for ctr2 in range( numberOfActions ):
				values.append( stateValue[ ctr2 ][ currentStateID ] )									#copy all values for current state
				maxValue = max( values )																#find the max value
			for ctr2 in range( numberOfActions ):														#loop to take care of multiple max values
				if values[ ctr2 ] == maxValue:
					maxActions.append( ctr2 )															#record actions having max value
			selectedAction = maxActions[ random.randint( 0, len( maxActions ) - 1 ) ]					#select among the above actions randomly
		else:																							#explore
			selectedAction = random.randint( 0, numberOfActions - 1 )
		
		[ nextEReward, atGoal, nextState, endOfEpisode, actionFailure ] = functionDict[ 'applyAction' ]( selectedAction, currentState, goals, allActions, representationDict, actionProbabilityThreshold, atGoal )
		nextStateID = functionDict[ 'getStateID' ]( representationDict, nextState)
			
		if nextStateID not in stateSpace:
			for ctr2 in range( numberOfOptions ):
				optionModel[ ctr2 ][ int( str( nextStateID ) + str( nextStateID ) ) ] = 0
				optionModel[ ctr2 ][ nextStateID ] = 0
				for ctr3 in range( len( stateSpace ) ):
					optionModel[ ctr2 ][ int( str( stateSpace[ ctr3 ] ) + str( nextStateID ) ) ] = 0
					optionModel[ ctr2 ][ int( str( nextStateID ) + str( stateSpace[ ctr3 ] ) ) ] = 0
			
			stateSpace.append( nextStateID )
			
			for ctr2 in range( numberOfActions + numberOfOptions ):
				stateValue[ ctr2 ][ nextStateID ] = 0													#initialisation of state action value
				for ctr3 in range( numberOfOptions ):
					optionValueFunction[ ctr3 ][ ctr2 ][ nextStateID ] = 0								#initialisation of option value function
				
		[ salientEventIndex, salientEventIndicator ] = functionDict[ 'isSalientEvent' ](  currentState, salientEvent, salientEventIndex )
		if salientEventIndicator:																		#indicates occurrence of a salient event
			
			if optionEventObserved[ salientEventIndex - 1 ] == 0:											#indicates first time seeing this event
				optionEventObserved[ salientEventIndex - 1 ] = 1
				addedOptionIndex = salientEventIndex - 1													#to identify option to skip in next block
				optionCounter = optionCounter + 1
				
				optionInitiationSet[ salientEventIndex - 1 ].append( currentStateID )							#adding s_t to initiation set !!check to see if s_t is already there
				optionTerminationStateSet[ salientEventIndex - 1 ].append( nextStateID )						#termination probability of these states is 1
				optionEventObserved[ salientEventIndex - 1 ] = 1
								
			
			nextIReward = tau * ( 1 - optionModel[ salientEventIndex - 1 ][ int( str( nextStateID ) + str( currentStateID ) ) ] )
			nextIReward = 0
		else:
			nextIReward = 0
		
		for ctr1 in range( numberOfOptions ):															#updating option models
			if optionEventObserved[ ctr1 ] == 1 and ctr1 != addedOptionIndex:							#do not iterate for just added option or option which has not yet been seen
				if nextStateID in optionInitiationSet[ ctr1 ]:											#growing initiation set
					if currentStateID not in optionInitiationSet[ ctr1 ]:
						optionInitiationSet[ ctr1 ].append( currentStateID )
					
				values = []
				maxActions = []
				for ctr2 in range( numberOfActions ):
					values.append( optionValueFunction[ ctr1 ][ ctr2 ][ currentStateID ] )
				maxValue = max( values )
					
				if values[ selectedAction ] == maxValue:												#selected action is greedy action for this option in state currentState
					if nextStateID in optionTerminationStateSet[ ctr1 ]:
						beta = 1
					else:
						beta = 0
							
					for state in stateSpace:
						delta = 0
						if state == nextStateID:
							delta = 1
								
						optionModel[ ctr1 ][ int( str( state ) + str( currentStateID ) ) ] = ( 1 - alpha ) * optionModel[ ctr1 ][ int( str( state ) + str( currentStateID ) ) ] + alpha * ( gamma * ( 1 -  beta ) * optionModel[ ctr1 ][ int( str( state ) + str( nextStateID ) ) ] + gamma * beta * delta )		#updating option model transition problability
							
						optionModel[ ctr1 ][ currentStateID ] = ( 1 - alpha ) * optionModel[ ctr1 ][ currentStateID ] + alpha * ( eReward + gamma * ( 1 - beta ) * optionModel[ ctr1 ][ nextStateID ] )																				#updating option model reward

						values = []
						
		values = []
		for ctr1 in range( numberOfActions + numberOfOptions ):
			values.append( stateValue[ ctr1 ][ nextStateID ] )
		maxValue = max( values )
	
		stateValue[ selectedAction ][ currentStateID ] = ( 1 - alpha ) * stateValue[ selectedAction ][ currentStateID ] + alpha * ( eReward + iReward + gamma * maxValue )

		for ctr1 in range( numberOfOptions ):
			if optionEventObserved[ ctr1 ] == 1:
				sum = 0
				for ctr2 in range( len( stateSpace ) ):
					values = []
					for ctr3 in range( numberOfActions + numberOfOptions ):
						values.append( stateValue[ ctr3 ][ stateSpace[ ctr2 ] ] )
					maxValue = max( values )
				
					sum = sum + optionModel[ ctr1 ][ int( str( stateSpace[ ctr2 ] ) + str( currentStateID ) ) ] * maxValue
			
				stateValue[ numberOfActions + ctr1 ][ currentStateID ] = ( 1 - alpha ) * stateValue[ numberOfActions + ctr1 ][ currentStateID ] + alpha * ( optionModel[ ctr1 ][ currentStateID ] + sum )
	
		for ctr1 in range( numberOfOptions ):
			if optionEventObserved[ ctr1 ] == 1 and currentState in optionInitiationSet[ ctr1 ]:
					
				if nextState in optionTerminationStateSet[ ctr1 ]:
					beta = 1
				else:
					beta = 0
				
				values = []
				for ctr2 in range( numberOfActions + numberOfOptions ):
					values.append( optionValueFunction[ ctr1 ][ ctr2 ][ nextStateID ] )
				maxValue = max( values )
					
				optionValueFunction[ ctr1 ][ selectedAction ][ currentStateID ] = ( 1 - alpha ) * optionValueFunction[ ctr1 ][ selectedAction ][ currentStateID ] + alpha * ( eReward +  gamma * beta * 1 + gamma * ( 1 - beta ) * maxValue )
					
				for ctr2 in range( numberOfOptions ):
					if optionEventObserved[ ctr2 ] == 1 and currentState in optionInitiationSet[ ctr2 ] and ctr1 != ctr2:
						sum = 0
						for ctr3 in range( len( stateSpace ) ):
							
							if stateSpace[ ctr3 ] in optionTerminationStateSet[ ctr1 ]:
								beta = 1
							else:
								beta = 0
								
							values = []
							for ctr4 in range( numberOfActions + numberOfOptions ):
								values.append( stateValue[ ctr4 ][ stateSpace[ ctr3 ] ] )
							maxValue = max( values )
								
							sum = sum + optionModel[ ctr2 ][ int( str( stateSpace[ ctr3 ] ) + str( currentStateID ) ) ] * ( beta * 1 + ( 1 - beta ) * maxValue ) 
							
						optionValueFunction[ ctr1 ][ numberOfActions + ctr2 ][ currentStateID ] = ( 1 - alpha ) * optionValueFunction[ ctr1 ][ numberOfActions + ctr2 ][ currentStateID ] + alpha * ( optionModel[ ctr2 ][ currentStateID ] + sum )

		eReward = nextEReward
		iReward = nextIReward
		currentState = nextState
		currentStateID = nextStateID
		
		actionCounter1 = actionCounter1 + 1
		actionCounter2 = actionCounter2 + 1

		if eReward == 1:
			eRewardCounter = eRewardCounter + 1
			actionsPerERewardCounter.append( actionCounter2 )
			actionCounter2 = 0
		
		if endOfEpisode:
			break
					
	opFile.write( '\nactionCounter1: ' + str( actionCounter1 ) + '\neRewardCounter: ' + str( eRewardCounter ) + '\nActionsPerERewardCounter:\n' + str( len( actionsPerERewardCounter ) ) + '\n' + str( actionsPerERewardCounter ) )
	opFile.write( '\nstateValue:\n' + str( stateValue ) + '\nnumberOfStates: ' + str( len( stateValue[ 0 ] ) ) )
	endTime = datetime.datetime.now()
	opFile.write( '\n\n\nendTime: ' + str( endTime ) + '\ntimeTaken: ' + str( endTime - startTime ) )
	opFile.close()

		
	return [ [ actionCounter1 ], [ eRewardCounter ] ]