import gym
import rospy
import roslaunch
import time
import numpy as np
# from ~/ros_workspaces/tf_ws/src/geometry2/tf2 import tf2 
import eigen
# import tf
from gym import utils, spaces
from gym_gazebo.envs import gazebo_env
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Point, Quaternion
from nav_msgs.msg import Odometry
from std_srvs.srv import Empty
from gazebo_msgs.srv import GetModelState
from sensor_msgs.msg import LaserScan
from gym.utils import seeding
from gazebo_msgs.srv import DeleteModel, SpawnModel

PI = 3.1415926535897


class GazeboGridTurtlebotLidarEnv(gazebo_env.GazeboEnv):

    def __init__(self):
        # rospy.init_node('robot', anonymous=True)
        # Launch the simulation with the given launchfile name
        gazebo_env.GazeboEnv.__init__(self, "GazeboGridTurtlebotLidar_v0.launch")
        self.vel_pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=5)
        self.unpause = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
        self.pause = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
        self.reset_proxy = rospy.ServiceProxy('/gazebo/reset_simulation', Empty)
        self.data = None

        rospy.wait_for_service("/gazebo/delete_model")
        rospy.wait_for_service("/gazebo/spawn_sdf_model")
        print("Got the Gazebo services running")
        self.delete_model = rospy.ServiceProxy("/gazebo/delete_model", DeleteModel)
        self.spawn_model = rospy.ServiceProxy("/gazebo/spawn_sdf_model", SpawnModel)
        self.spawn_urdf_model = rospy.ServiceProxy("/gazebo/spawn_urdf_model", SpawnModel)

        #objects co-ordinates:
        rospy.wait_for_service("/gazebo/get_model_state")
        self.model_cords = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
        #list of objects in the enviroment including the robot:
        self.objects_dict = {'cube1':['unit_box', 'link'], 'cube2': ['unit_box_0', 'link'], 'turtlebot':['mobile_base','']}
        # Robot postion:
        self.pos = None
        self.quat = None
        self.action_space = spaces.Discrete(4) #FW,TL,TR,LI
        self.reward_range = (-np.inf, np.inf)
        self.direction = 0
        # light param:
        self.light = 0
        # door params
        self.up_door    =   "up_door"
        self.down_door  =   "down_door"
        self.cube = "cube"
        self.up_door_pose  =   Pose(Point(x=4.01, y=5.01,  z=0),   Quaternion(x=0,y=0,z=0,w=1) )
        self.down_door_pose  =   Pose(Point(x=4.01, y=3.99,  z=0),   Quaternion(x=0,y=0,z=0,w=1) )
        with open("/home/cgnarendiran/.gazebo/models/drc_practice_hinged_door/model.sdf", "r") as f:
            self.product_xml = f.read()
        with open("/home/cgnarendiran/softwares/gym-gazebo/gym_gazebo/envs/assets/worlds/cube.urdf", "r") as f:
            self.cube_xml = f.read()
        self.down_door_open = 0
        self.up_door_open = 0
        self._seed()
        # self.subs = rospy.Subscriber('/odom', Odometry, self.quat_callback)
        # rospy.spin()


    def quat_callback(self,data):
        # print("in the callback")
        self.quat = data.pose.pose.orientation


    def _observe(self):
        observations = []
        done = False
        if self.down_door_open ==1:
            self.spawn_model(self.down_door, self.product_xml, "", self.up_door_pose, "world")
        if self.up_door_open ==1:
            self.spawn_model(self.up_door, self.product_xml, "", self.down_door_pose, "world")
        self.down_door_open = 0
        self.up_door_open = 0
        
        # discretized_ranges = []
        # min_range = 0.1
        # mod = len(data.ranges)/new_ranges
        for obj in sorted(self.objects_dict.keys()):
            observations.append(round(self.model_cords(self.objects_dict[obj][0], self.objects_dict[obj][1] ).pose.position.x, 1))
            observations.append(round(self.model_cords(self.objects_dict[obj][0], self.objects_dict[obj][1] ).pose.position.y, 1))  


        # Determine the position of the robot at run-time:
        self.pos = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.position

        if round(self.pos.x,1) ==4.5 and round(self.pos.y,1) ==3.5:
            if self.direction == 3:
                if self.light == 1:
                    self.delete_model(self.down_door)
                    self.down_door_open = 1

        # self.pos = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.position
        if round(self.pos.x,1) ==4.5 and round(self.pos.y,1) ==4.5:
            if self.direction == 3:
                # self.spawn_model(self.item_name, self.product_xml, "", self.item_pose, "world")
                self.delete_model(self.up_door)
                self.up_door_open = 1
                # reward = 10
            if self.direction == 1:
                # self.spawn_model(self.item_name, self.product_xml, "", self.item_pose, "world")
                self.delete_model(self.down_door)
                self.down_door_open = 1
                # reward = 10

        if round(self.pos.x,1) ==4.5 and round(self.pos.y,1) == 5.5:
            if self.direction == 1:
                if self.light == 1:
                    self.delete_model(self.up_door)
                    self.up_door_open = 1


        # direction of the robot:
        # global yaw
        # yaw = 0
        # direction = 0
        # ori = self.model_cords(objects_dict['turtlebot'][0],objects_dict['turtlebot'][1]).pose.orientation
        # orientation_list = [ori.x, ori.y, ori.z, ori.w]
        # # (_, _, yaw) = tf.transformations.euler_from_quaternion (orientation_list)
        # if -0.05<=round(yaw,2)<=0.05:
        #     direction = 1
        # elif 1.52<=round(yaw,2)<=1.62:
        #     direction = 0
        # elif 3.09<=round(yaw,2)<=3.19:
        #     direction = 3
        # elif 4.64<=round(yaw,2)<=4.76:
        #     direction = 2
        observations.append(self.direction) 
        #append goal cords
        observations.append(4.5)
        observations.append(4.5)
        # append door conditions:
        observations.append(self.down_door_open)
        observations.append(self.up_door_open)

        # reset the light:
        if self.light == 1:
            self.light = 0
            self.delete_model(self.cube)
        
        print(observations)
        return observations,done

        # laser scan observations:
        # for i, item in enumerate(data.ranges):
        #     if (i%mod==0):
        #         if data.ranges[i] == float ('Inf'):
        #             discretized_ranges.append(6)
        #         elif np.isnan(data.ranges[i]):
        #             discretized_ranges.append(0)
        #         else:
        #             discretized_ranges.append(int(data.ranges[i]))
        #     if (min_range > data.ranges[i] > 0):
        #         done = True
        # return discretized_ranges,done


    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]


    def _step(self, action):
        reward = 0
        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            self.unpause()
        except (rospy.ServiceException) as e:
            print ("/gazebo/unpause_physics service call failed")
        #read laser data
        while self.data is None:
            try:
                self.data = rospy.wait_for_message('/scan', LaserScan, timeout=5)
            except:
                pass
        # Print the action:
        print("Action=",action)

        if action == 0: #FORWARD
            now = rospy.get_rostime().to_sec()
            vel_cmd = Twist()
            vel_cmd.linear.x = 0.4
            vel_cmd.angular.z = 0.0
            while rospy.get_rostime().to_sec()< now+0.25:
                self.vel_pub.publish(vel_cmd)

            # check for collision with wall:
            # print(self.data.ranges)
            self.pos = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.position
            print(self.pos)
            if round(self.pos.x,1) <= 0.4 or round(self.pos.y,1) <= 0.4  or round(self.pos.x,1) >= 7.6 or round(self.pos.y,1) >= 7.6:
            # if (self.data.ranges[int(round(len(self.data.ranges)/2))]<=0.25):
                print("faced a wall")
                vel_cmd.linear.x = -0.4
                vel_cmd.angular.z = 0.0
                reward -= 25
                now = rospy.get_rostime().to_sec()
                while rospy.get_rostime().to_sec()< now+0.25:
                    self.vel_pub.publish(vel_cmd)
            else:
                print("all clear")
                now = rospy.get_rostime().to_sec()
                while rospy.get_rostime().to_sec()< now+2.25:
                    self.vel_pub.publish(vel_cmd)   

        elif action == 1: #TURNLEFT
            self.direction-=1
            if self.direction==-1:
                self.direction=3
            vel_cmd = Twist()
            vel_cmd.linear.x = 0.0
            vel_cmd.angular.z = PI/8

            # Get Quaternions using odometry
            # self.subs = rospy.Subscriber('/odom', Odometry, self.quat_callback)
            # print(self.quat)

            # Get Quaternions using Gazebo
            self.quat = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.orientation

            q = eigen.Quaterniond(eigen.Vector4d(self.quat.x, self.quat.y, self.quat.z, self.quat.w)) # Eigen::Quaterniond(Eigen::Vector4d) (x, y, z, w)
            rpy = q.toRotationMatrix().eulerAngles(0, 1, 2)
            yaw = np.float(np.array(rpy)[2])
            # Track the last angle measured
            last_angle = yaw

            # Track how far we have turned
            turn_angle = 0

            angular_tolerance = 0.0001
            goal_angle = PI/2

            # Begin the rotation
            while abs(turn_angle + angular_tolerance) < abs(goal_angle) and not rospy.is_shutdown():
                # Publish the Twist message and sleep 1 cycle         
                self.vel_pub.publish(vel_cmd)
                
                # Get the current rotation

                # Get Quaternions using odometry
                # self.subs = rospy.Subscriber('/odom', Odometry,self.quat_callback)

                # Get Quaternions using Gazebo
                self.quat = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.orientation
                q = eigen.Quaterniond(eigen.Vector4d(self.quat.x, self.quat.y, self.quat.z, self.quat.w)) # Eigen::Quaterniond(Eigen::Vector4d) (x, y, z, w)
                rpy = q.toRotationMatrix().eulerAngles(0, 1, 2)
                yaw = np.float(np.array(rpy)[2])
                # Compute the amount of rotation since the last lopp
                delta_angle = yaw - last_angle
                turn_angle += delta_angle
                last_angle = yaw

            # Use time to monitor, open loop control:
            # now = rospy.get_rostime().to_sec()
            # while rospy.get_rostime().to_sec()< now+4.0:
            #     self.vel_pub.publish(vel_cmd)

        elif action == 2: #TURNRIGHT
            self.direction+=1
            if self.direction==4:
                self.direction=0
            vel_cmd = Twist()
            vel_cmd.linear.x = 0.0
            vel_cmd.angular.z = -PI/8

            # Get Quaternions using odometry
            # self.subs = rospy.Subscriber('/odom', Odometry,self.quat_callback)

            # Get Quaternions using Gazebo
            self.quat = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.orientation
            q = eigen.Quaterniond(eigen.Vector4d(self.quat.x, self.quat.y, self.quat.z, self.quat.w)) # Eigen::Quaterniond(Eigen::Vector4d) (x, y, z, w)
            rpy = q.toRotationMatrix().eulerAngles(0, 1, 2)
            yaw = np.float(np.array(rpy)[2])
            # Track the last angle measured
            last_angle = yaw

            # Track how far we have turned
            turn_angle = 0

            angular_tolerance = 0.0001
            goal_angle = PI/2

            # Begin the rotation
            while abs(turn_angle + angular_tolerance) < abs(goal_angle) and not rospy.is_shutdown():
                # Publish the Twist message and sleep 1 cycle         
                self.vel_pub.publish(vel_cmd)
                
                # Get the current rotation
                
                # Get Quaternions using odometry
                # self.subs = rospy.Subscriber('/odom', Odometry,self.quat_callback)
                # print(self.quat)

                # Get Quaternions using Gazebo
                self.quat = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.orientation
                q = eigen.Quaterniond(eigen.Vector4d(self.quat.x, self.quat.y, self.quat.z, self.quat.w)) # Eigen::Quaterniond(Eigen::Vector4d) (x, y, z, w)
                rpy = q.toRotationMatrix().eulerAngles(0, 1, 2)
                yaw = np.float(np.array(rpy)[2])
                # Compute the amount of rotation since the last lopp
                delta_angle = yaw - last_angle
                turn_angle += delta_angle
                last_angle = yaw

            # Use time to monitor, open loop control:
            # now = rospy.get_rostime().to_sec()
            # while rospy.get_rostime().to_sec()< now+4.0:
            #     self.vel_pub.publish(vel_cmd)

        else: #LIGHT
            self.light =1
            self.spawn_urdf_model(self.cube, self.cube_xml, '', Pose(Point(x=0.01, y=0.0,  z=0.43),   Quaternion(x=0,y=0,z=0,w=1) ), 'mobile_base::base_footprint')
            rospy.sleep(1.5)

        rospy.wait_for_service('/gazebo/pause_physics')
        try:
            #resp_pause = pause.call()
            self.pause()
        except (rospy.ServiceException) as e:
            print ("/gazebo/pause_physics service call failed")

        # Observe and record the states:
        state,done = self._observe()
        

        # reward for reaching goal position:
        self.pos = self.model_cords(self.objects_dict['turtlebot'][0], self.objects_dict['turtlebot'][1]).pose.position
        if round(self.pos.x,1) ==4.5 and round(self.pos.y,1) ==4.5:
            reward += 100
        if not done:
            reward += 0
        else:
            reward -= 200

        return state, reward, done, {}

    def _reset(self):

        # Resets the state of the environment and returns an initial observation.
        rospy.wait_for_service('/gazebo/reset_simulation')
        try:
            #reset_proxy.call()
            self.reset_proxy()
        except (rospy.ServiceException) as e:
            print ("/gazebo/reset_simulation service call failed")

        # Unpause simulation to make observation
        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            #resp_pause = pause.call()
            self.unpause()
        except (rospy.ServiceException) as e:
            print ("/gazebo/unpause_physics service call failed")

        try:
            #spawn the door()
            # self.spawn_model(, self.product_xml, "", item_pose, "world")
            self.spawn_model(self.up_door, self.product_xml, "", self.up_door_pose, "world")
            self.spawn_model(self.down_door, self.product_xml, "", self.down_door_pose, "world")
        except (rospy.ServiceException) as e:
            print ("/gazebo/unpause_physics service call failed")
 
        #read laser data
        while self.data is None:
            try:
                self.data = rospy.wait_for_message('/scan', LaserScan, timeout=5)
            except:
                pass

        rospy.wait_for_service('/gazebo/pause_physics')
        try:
            #resp_pause = pause.call()
            self.pause()
        except (rospy.ServiceException) as e:
            print ("/gazebo/pause_physics service call failed")

        state = self._observe()
        return state