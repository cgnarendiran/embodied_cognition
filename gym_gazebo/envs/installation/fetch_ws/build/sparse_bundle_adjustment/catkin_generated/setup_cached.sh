#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/cgnarendiran/softwares/gym-gazebo/gym_gazebo/envs/installation/fetch_ws/devel/.private/sparse_bundle_adjustment:$CMAKE_PREFIX_PATH"
export PWD="/home/cgnarendiran/softwares/gym-gazebo/gym_gazebo/envs/installation/fetch_ws/build/sparse_bundle_adjustment"
export ROSLISP_PACKAGE_DIRECTORIES="/home/cgnarendiran/softwares/gym-gazebo/gym_gazebo/envs/installation/fetch_ws/devel/.private/sparse_bundle_adjustment/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/cgnarendiran/softwares/gym-gazebo/gym_gazebo/envs/installation/fetch_ws/src/sparse_bundle_adjustment:$ROS_PACKAGE_PATH"