(define (problem test1)
	(:domain traversal)
	
	(:objects
		x1 x2 x3 x4 x5 x6 x7 x8 - xcoord
		y1 y2 y3 y4 y5 y6 y7 y8 - ycoord
		r - robot
		d1 d2 - door)
	
	(:init
		(leftOf x1 x2)
		(leftOf x2 x3)
		(leftOf x3 x4)
		(leftOf x4 x5)
		(leftOf x5 x6)
		(leftOf x6 x7)
		(leftOf x7 x8)
		
		(above y8 y7)
		(above y7 y6)
		(above y6 y5)
		(above y5 y4)
		(above y4 y3)
		(above y3 y2)
		(above y2 y1)
		
		(empty x1 y1)
		(empty x1 y2)
		(empty x1 y3)
		(empty x1 y4)
		(empty x1 y5)
		(empty x1 y6)
		(empty x1 y7)
		(empty x1 y8)
		
		(empty x2 y1)
		(empty x2 y2)
		(empty x2 y3)
		(empty x2 y4)
		(empty x2 y5)
		(empty x2 y6)
		(empty x2 y7)
		(empty x2 y8)
		
		(empty x3 y1)
		(empty x3 y2)
		(empty x3 y3)
		(empty x3 y4)
		(empty x3 y5)
		(empty x3 y6)
		(empty x3 y7)
		(empty x3 y8)
		
		(empty x4 y1)
		(empty x4 y2)
		(empty x4 y3)
		(empty x4 y4)
		(empty x4 y5)
		(empty x4 y6)
		(empty x4 y7)
		(empty x4 y8)
		
		(empty x5 y1)
		(empty x5 y2)
		(empty x5 y3)
		(empty x5 y4)
		(empty x5 y8)
		
		(empty x6 y1)
		(empty x6 y2)
		(empty x6 y3)
		(empty x6 y6)
		(empty x6 y8)
		
		(empty x7 y1)
		(empty x7 y2)
		(empty x7 y3)
		(empty x7 y4)
		(empty x7 y8)
		
		(empty x8 y1)
		(empty x8 y2)
		(empty x8 y3)
		(empty x8 y4)
		(empty x8 y5)
		(empty x8 y6)
		(empty x8 y7)
		(empty x8 y8)
		
		(robotAt r x6 y4)
		(facingN r)
		
		(doorAt d1 x6 y5)
		(doorAt d2 x6 y7)
		(doorClosed d1)
		(doorClosed d2)
		)
	
	(:goal
			(robotAt r x6 y6)
		))