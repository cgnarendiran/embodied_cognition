(define (problem test1)
	(:domain traversal)
	
	(:objects
		x1 x2 x3 x4 - xcoord
		y1 y2 y3 y4 - ycoord
		r - robot
		d - door)
	
	(:init
		(leftOf x1 x2)
		(leftOf x2 x3)
		(leftOf x3 x4)
		
		(above y4 y3)
		(above y3 y2)
		(above y2 y1)
		
		(empty x1 y1)
		(empty x1 y2)
		(empty x1 y3)
		(empty x1 y4)
		
		(empty x2 y1)
		
		(empty x3 y3)
		
		(empty x4 y1)
				
		(robotAt r x3 y1)
		(facingN r)
		
		(doorAt d x3 y2)
		(doorClosed d)
		)
	
	(:goal
			(robotAt r x3 y3)
		))