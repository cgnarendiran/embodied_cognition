#This code is used to get the input from the pddl file

import pddlpy																						#existing library used to parse pddl file
import itertools

def getPDDLInput( domainFile, problemFile ):
	
	problem = pddlpy.DomainProblem( domainFile, problemFile )

	objectDict = problem.worldobjects()																#all objects in the world
	initState = problem.initialstate()																#initial state of the problem

	goalData = problem.goals()																		#goal information

	initialState = []																				#converting initial state and goal information to string format
	goals = []

	for state in initState:
		newString = str( state )
		newString = newString.replace("'","")
		newString = newString.replace("(","")
		newString = newString.replace(")","")
		newString = newString.replace(" ","")
		initialState.append( newString )

	for goal in goalData:
		newString = str( goal )
		newString = newString.replace("'","")
		newString = newString.replace("(","")
		newString = newString.replace(")","")
		newString = newString.replace(" ","")
		goals.append( newString )		
	
	actions = []
	allActions = []
	operators = problem.operators()																	#all operators

	for op in list( operators ):																	#from the above operators, listing out the grounded versions along with preconditions and effects
		problem = pddlpy.DomainProblem( domainFile, problemFile )
		numActions = len( list( problem.ground_operator( op ) ) )
		for num in range( numActions ):
			missingAction = False
			newAction = []
			newAction.append( op )
			
			posEffects = []
			negEffects = []
			vl = []
			
			posPreConditions = list( problem.ground_operator( op ) )[ num ].precondition_pos
			temp1 = list( posPreConditions )
			temp2 = []
			for item in temp1:
				newString = str( item )
				newString = newString.replace("'","")
				newString = newString.replace("(","")
				newString = newString.replace(")","")
				newString = newString.replace(" ","")
				newString = newString.strip( '{' )
				newString = newString.strip( '}' )
				if newString == 'missing,' :
					missingAction = True
				else:
					temp2.append( newString )
			newAction.append( temp2 )
			
			posEffects = list( problem.ground_operator( op ) )[ num ].effect_pos
			temp1 = list( posEffects )
			temp2 = []
			for item in temp1:
				newString = str( item )
				newString = newString.replace("'","")
				newString = newString.replace("(","")
				newString = newString.replace(")","")
				newString = newString.replace(" ","")
				newString = newString.strip( '{' )
				newString = newString.strip( '}' )
				temp2.append( newString )
			newAction.append( temp2	)
			
			negEffects = list( problem.ground_operator( op ) )[ num ].effect_neg
			temp1 = list( negEffects )
			temp2 = []
			for item in temp1:
				newString = str( item )
				newString = newString.replace("'","")
				newString = newString.replace("(","")
				newString = newString.replace(")","")
				newString = newString.replace(" ","")
				newString = newString.strip( '{' )
				newString = newString.strip( '}' )
				temp2.append( newString )
			newAction.append( temp2	)
			allActions.append( newAction )
			if not missingAction:
				actions.append( newAction )
	
	file = open( domainFile, 'r' )																	#additional code to acquire the predicate information
	pddlText = file.read()
	file.close()
	
	startLocation = pddlText.find( ':predicates' )
	endLocation = pddlText.find( ':action' )
	endLocation = endLocation - 2
	
	predicates = []
	nextPredicateFound = True
	
	while nextPredicateFound:
		predicateStartLocation = pddlText[ startLocation:endLocation ].find( '(' )
		predicateEndLocation = pddlText[ startLocation:endLocation ].find( ')' )
		
		if predicateStartLocation == -1 or predicateEndLocation == -1:
			nextPredicateFound = False
			break
		
		newPredicate = []
		tempList = pddlText[ startLocation + predicateStartLocation + 1:startLocation + predicateEndLocation ].split( ' ' )
		newPredicate.append( tempList[ 0 ] )
		startLocation = startLocation + predicateEndLocation + 1
		
		lengthCounter = 1
		
		while lengthCounter < len( tempList ) - 1:
			argumentCounter = 0
			while lengthCounter < len( tempList ):
				if tempList[ lengthCounter ].find( '?' ) == -1:
					break
				else:
					argumentCounter = argumentCounter + 1
					lengthCounter = lengthCounter + 1
			
			lengthCounter = lengthCounter + 1
			newPredicate.append( argumentCounter )
			newPredicate.append( tempList[ lengthCounter ] )
			lengthCounter = lengthCounter + 1
		
		predicates.append( newPredicate )

	objectList = []																					#list of object types and their values
	keyList = list( objectDict.keys() )
	valueList = list( objectDict.values() )
	valueSet = set( valueList )
	
	for element in valueSet:
		newObject = []
		newObject.append( element )
		while True:
			if element in valueList:
				index = valueList.index( element )
				newObject.append( keyList[ index ] )
				keyList.pop( index )
				valueList.pop( index )
			else:
				break
		objectList.append( newObject )		
	representationDict = {}																			#dictionary listing all combinations of individual predicates
	representationDictCount = 0
	
	for predicate in predicates:
		comboNum = []
		comboObject = []
		comboLists = []
		comboLists2 = []
		
		count = 0
		predicateName = predicate[ count ]
		count = count + 1
		while count < len( predicate ):
			comboNum.append( predicate[ count ] )
			count = count + 1
			comboObject.append( predicate[ count ] )
			count = count + 1
		
		count1 = 0
		while count1 < len( comboNum ):
			count2 = 0
			while count2 < len( objectList ):
				if comboObject[ count1 ] in objectList[ count2 ]:
					break
				else:
					count2 = count2 + 1
			
			if comboNum[ count1 ] == 1:
				comboLists.append( [] )
				comboLists2.append(objectList[ count2 ][ 1: ] )
			else:
				comboLists.append( list( itertools.permutations( objectList[ count2 ][ 1: ], comboNum[ count1 ] ) ) )
				comboLists2.append( [] )
			count1 = count1 + 1
			
		count1 = 0
		while count1 < len( comboNum ):
			if comboNum[ count1 ] > 1:
				for item in comboLists[ count1 ]:
					tempString = ''
					for count3 in range( 0, comboNum[ count1 ] ):
						tempString = tempString + str( item[ count3 ] ) + ','
					tempString = tempString[ 0:-1 ]
					comboLists2[ count1 ].append( tempString )
			count1 = count1 + 1
			
		comboLists = []
		
		if len( comboNum ) == 1:
			comboLists = comboLists2[ 0 ]
		else:
			if len( comboNum ) == 2:
				for item1 in comboLists2[ 0 ]:
					for item2 in comboLists2[ 1 ]:
						comboLists.append( item1 + ',' + item2 )
			else:
				if len( comboNum ) == 3:
					for item1 in comboLists2[ 0 ]:
						for item2 in comboLists2[ 1 ]:
							for item3 in comboLists2[ 2 ]:
								comboLists.append( item1 + ',' + item2 + ',' + item3 )
				else:
					if len( comboNum ) == 4:
						for item1 in comboLists2[ 0 ]:
							for item2 in comboLists2[ 1 ]:
								for item3 in comboLists2[ 2 ]:
									for item4 in comboLists2[ 3 ]:
										comboLists.append( item1 + ',' + item2 + ',' + item3 + ',' + item4 )
		
		for item in comboLists:
			representationDict[ predicateName + ',' + item ] = representationDictCount
			representationDictCount = representationDictCount + 1
	
	return( [ objectDict, initialState, goals, actions, allActions, predicates, representationDict ] )