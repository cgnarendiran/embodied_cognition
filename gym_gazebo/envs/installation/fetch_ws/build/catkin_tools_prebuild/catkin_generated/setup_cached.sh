#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/cgnarendiran/softwares/gym-gazebo/gym_gazebo/envs/installation/fetch_ws/devel/.private/catkin_tools_prebuild:$CMAKE_PREFIX_PATH"
export PATH="/home/cgnarendiran/softwares/gym-gazebo/gym_gazebo/envs/installation/catkin_ws/devel/bin:/opt/ros/kinetic/bin:/usr/bin:/home/cgnarendiran/.pyenv/libexec:/home/cgnarendiran/.pyenv/plugins/python-build/bin:/usr/bin:/home/cgnarendiran/.pyenv/libexec:/home/cgnarendiran/.pyenv/plugins/python-build/bin:/home/cgnarendiran/.pyenv/shims:/home/cgnarendiran/.pyenv/bin:/usr/local/cuda-8.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export ROSLISP_PACKAGE_DIRECTORIES="/home/cgnarendiran/softwares/gym-gazebo/gym_gazebo/envs/installation/fetch_ws/devel/.private/catkin_tools_prebuild/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/cgnarendiran/softwares/gym-gazebo/gym_gazebo/envs/installation/fetch_ws/build/catkin_tools_prebuild:$ROS_PACKAGE_PATH"