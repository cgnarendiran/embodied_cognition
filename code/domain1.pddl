(define (domain traversal)
	(:requirements	:strips	:typing)
	(:types
		robot
		xcoord
		ycoord
		door)
	
	(:predicates
		(robotAt ?r - robot ?x - xcoord ?y - ycoord)
		(doorAt ?d - door ?x - xcoord ?y - ycoord)
		(doorOpen ?d - door)
		(doorClosed ?d - door)
		(empty ?x - xcoord ?y - ycoord)
		(leftOf ?x1 ?x2 - xcoord)
		(above ?y1 ?y2 - ycoord)
		(facingN ?r - robot)
		(facingS ?r - robot)
		(facingE ?r - robot)
		(facingW ?r - robot)
	)
	
	(:action moveEast
		:parameters (?r - robot ?x1 ?x2 - xcoord ?y - ycoord)
		:precondition (and (robotAt ?r ?x1 ?y) (facingE ?r) (leftOf ?x2 ?x1) (empty ?x2 ?y ))
		:effect (and (robotAt ?r ?x2 ?y) (empty ?x1 ?y) (not (empty ?x2 ?y)) (not (robotAt ?r ?x1 ?y))))
	
	(:action moveWest
		:parameters (?r - robot ?x1 ?x2 - xcoord ?y - ycoord)
		:precondition (and (robotAt ?r ?x1 ?y) (facingW ?r) (leftOf ?x1 ?x2) (empty ?x2 ?y ))
		:effect (and (robotAt ?r ?x2 ?y) (empty ?x1 ?y) (not (empty ?x2 ?y)) (not (robotAt ?r ?x1 ?y))))
	
	(:action moveNorth
		:parameters (?r - robot ?x - xcoord ?y1 ?y2 - ycoord)
		:precondition (and (robotAt ?r ?x ?y1) (facingN ?r) (above ?y2 ?y1) (empty ?x ?y2 ))
		:effect (and (robotAt ?r ?x ?y2) (empty ?x ?y1) (not (empty ?x ?y2)) (not (robotAt ?r ?x ?y1))))
	
	(:action moveSouth
		:parameters (?r - robot ?x - xcoord ?y1 ?y2 - ycoord)
		:precondition (and (robotAt ?r ?x ?y1) (facingS ?r) (above ?y1 ?y2) (empty ?x ?y2 ))
		:effect (and (robotAt ?r ?x ?y2) (empty ?x ?y1) (not (empty ?x ?y2)) (not (robotAt ?r ?x ?y1))))
	
	(:action turnLeftNorth
		:parameters (?r - robot)
		:precondition (facingN ?r)
		:effect (and (facingW ?r) (not (facingN ?r))))
		
	(:action turnLeftSouth
		:parameters (?r - robot)
		:precondition (facingS ?r)
		:effect (and (facingE ?r) (not (facingS ?r))))	
	
	(:action turnLeftEast
		:parameters (?r - robot)
		:precondition (facingE ?r)
		:effect (and (facingN ?r) (not (facingE ?r))))
	
	(:action turnLeftWest
		:parameters (?r - robot)
		:precondition (facingW ?r)
		:effect (and (facingS ?r) (not (facingW ?r))))
	
	(:action turnRightNorth
		:parameters (?r - robot)
		:precondition (facingN ?r)
		:effect (and (facingE ?r) (not (facingN ?r))))
		
	(:action turnRightSouth
		:parameters (?r - robot)
		:precondition (facingS ?r)
		:effect (and (facingW ?r) (not (facingS ?r))))	
	
	(:action turnRightEast
		:parameters (?r - robot)
		:precondition (facingE ?r)
		:effect (and (facingS ?r) (not (facingE ?r))))
	
	(:action turnRightWest
		:parameters (?r - robot)
		:precondition (facingW ?r)
		:effect (and (facingN ?r) (not (facingW ?r))))
		
	(:action openDoorNorth
		:parameters (?r - robot ?d - door ?x - xcoord ?y1 ?y2 ?y3 - ycoord)
		:precondition (and (missing) (robotAt ?r ?x ?y1) (facingN ?r) (above ?y2 ?y1) (above ?y3 ?y2) (doorAt ?d ?x ?y2) (doorClosed ?d))
		:effect (and (doorOpen ?d) (empty ?x ?y2) (not (doorClosed ?d)) (not (empty ?x ?y3))))
	
	(:action openDoorSouth
		:parameters (?r - robot ?d - door ?x - xcoord ?y1 ?y2 - ycoord)
		:precondition (and (missing) (robotAt ?r ?x ?y1) (facingS ?r) (above ?y1 ?y2) (above ?y2 ?y3) (doorAt ?d ?x ?y2) (doorClosed ?d))
		:effect (and (doorOpen ?d) (empty ?x ?y2) (not (doorClosed ?d)) (not (empty ?x ?y3))))
	
	(:action openDoorEast
		:parameters (?r - robot ?d - door ?x1 ?x2 - xcoord ?y - ycoord)
		:precondition (and (missing) (robotAt ?r ?x1 ?y) (facingE ?r) (leftOf ?x2 ?x1) (leftOf ?x3 ?x2) (doorAt ?d ?x2 ?y) (doorClosed ?d))
		:effect (and (doorOpen ?d) (empty ?x2 ?y) (not (doorClosed ?d)) (not (empty ?x3 ?y))))
	
	(:action openDoorWest
		:parameters (?r - robot ?d - door ?x1 ?x2 - xcoord ?y - ycoord)
		:precondition (and (missing) (robotAt ?r ?x1 ?y) (facingW ?r) (leftOf ?x1 ?x2) (leftOf ?x2 ?x3) (doorAt ?d ?x2 ?y) (doorClosed ?d))
		:effect (and (doorOpen ?d) (empty ?x2 ?y) (not (doorClosed ?d)) (not (empty ?x3 ?y))))
	
	(:action moveThroughDoorNorth
		:parameters (?r - robot ?d - door ?x - xcoord ?y1 ?y2 - ycoord)
		:precondition (and (robotAt ?r ?x ?y1) (doorAt ?d ?x ?y1) (doorOpen ?d) (facingN ?r) (above ?y2 ?y1))
		:effect (and (robotAt ?r ?x ?y2) (doorClosed ?d) (not (doorOpen ?d)) (not (empty ?x ?y1))))
	
	(:action moveThroughDoorSouth
		:parameters (?r - robot ?d - door ?x - xcoord ?y1 ?y2 - ycoord)
		:precondition (and (missing) (robotAt ?r ?x ?y1) (doorAt ?d ?x ?y1) (doorOpen ?d) (facingS ?r) (above ?y1 ?y2))
		:effect (and (robotAt ?r ?x ?y2) (doorClosed ?d) (not (doorOpen ?d)) (not (empty ?x ?y1))))
	
	(:action moveThroughDoorEast
		:parameters (?r - robot ?d - door ?x1 ?x2 - xcoord ?y - ycoord)
		:precondition (and (missing) (robotAt ?r ?x1 ?y) (doorAt ?d ?x1 ?y) (doorOpen ?d) (facingE ?r) (leftOf ?x2 ?x1))
		:effect (and (robotAt ?r ?x2 ?y) (doorClosed ?d) (not (doorOpen ?d)) (not (empty ?x1 ?y))))
	
	(:action moveThroughDoorWest
		:parameters (?r - robot ?d - door ?x1 ?x2 - xcoord ?y - ycoord)
		:precondition (and (missing) (robotAt ?r ?x1 ?y) (doorAt ?d ?x1 ?y) (doorOpen ?d) (facingW ?r) (leftOf ?x1 ?x2))
		:effect (and (robotAt ?r ?x2 ?y) (doorClosed ?d) (not (doorOpen ?d)) (not (empty ?x1 ?y))))
	)
