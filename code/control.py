#This code acts to control execution of the different components

import pddlInput																					#existing library used to parse pddl file
import bfbsp																						#backward search planning
import time	
import rlEnv																						#reinforcement learning environment

domainFile = 'domain1.pddl'																			#pddl domain file
problemFile = 'problem2.pddl'																		#pddl problem file
planningNodeLimit = 1000000																			#number of nodes to expand during search

print( 'Processing problem files' )

[ objectDict, initialState, goals, actions, allActions, predicates, representationDict ] = pddlInput.getPDDLInput( domainFile, problemFile )		#reading pddl files
print( 'Processing complete' )

planSuccess = False
initial = True

while not planSuccess:
	print( 'Planning' )
	[ plan, literalDict ] = bfbsp.bfs( initialState, goals, actions, planningNodeLimit )			#executing planner
	
	
	
#	plan = []
#	literalDict = { 'doorOpen,d': 5 }	
	
	
	
	

	if plan == []:
		initial = False
		maxFrequency = max( literalDict.keys() )
		print( 'Unable to find a plan' )
	
		while len( literalDict ) > 0:																#identifying literal with maximum frequency
			maxFrequency = max( literalDict.values() )
			selected = ''
			for literal in literalDict:
				if literalDict[ literal ] == maxFrequency:
					selected = str( literal )
		
			literalDict.pop( selected )

			print( 'Attempting to learn an affordance corresponding to condition: ' + str( selected ) )		

			[ result, learnedOperators ] = rlEnv.executeIMRLExp( list( initialState ), [ selected ], [ selected ], allActions, representationDict, 1 )				#executing imrl

			if result:
				print( 'Successfully learned affordance corresponding to condition: ' + str( selected ) )
				print( 'Adding corresponding operator to planner and initiating re-planning' )
				for op in learnedOperators:
					actions.append( op )																									#adding learned grounded operator to list of known actions
				break
			else:
				print( 'Unable to learn affordance corresponding to condition: ' + str( selected ) )
				if len( literalDict ) > 0:
					print( 'Trying next suggested condition' )
				else:
					print( 'No more literal suggestions left.\nUnable to solve given problem.' )
	else:
		planSuccess = True
		print( 'Planning completed' )
		print( 'Plan:' )
		for action in plan:
			print( actions[ action ] )
		if initial:			
			print( 'Now executing plan' )
#			for action in plan:
#				"execute actions[ action ][ 0 ]"
			print( 'Plan executed' )	

