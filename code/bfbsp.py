#Implementation of the backward search planning algorithm augmented with heuristic for planning failure analysis
	
def checkGoal( initialState, goalSet ):															#check to see if goal is achieved
	flag = True
	for condition in goalSet:
		if condition not in initialState:
			flag = False
			break
	return flag

def getPlan( node, pathDict ):																	#recover plan on reaching successfull node in search tree
	nodeID = node[ 0 ]
	plan = []
	while( nodeID != 0 ):
		plan.append( pathDict[ nodeID ][ 1 ] )
		nodeID = pathDict[ nodeID ][ 0 ]		
	return plan

def getRelevantActions( goalSet, actions ):														#returns relevant actions for a set of goals
	relevantActions = []
	
	counter = 0
	while counter < len( actions ):
		for effect in actions[ counter ][ 2 ]:
			if effect in goalSet:
				flag = True
				for effect in actions[ counter ][ 3 ]:
					if effect in goalSet:
						flag = False
						break
				if flag:
					relevantActions.append( counter )
		counter = counter + 1
	return relevantActions

def applyAction( action, goalSet, allGoals, actions ):											#returns the next goal set on applying a relevant action to the current goal set
	nextGoalSet = []
	
	for goal in goalSet:
		if goal not in actions[ action ][ 2 ]:
			nextGoalSet.append( goal )
	
	for condition in actions[ action ][ 1 ]:
		if condition not in nextGoalSet:
			nextGoalSet.append( condition )
	
	for goals in allGoals:
		flag = True
		for goal in nextGoalSet:
			if goal not in goals:
				flag = False
				break
		if flag:
			nextGoalSet = []
			break

	return nextGoalSet

def bfs( initialState, goalSet, actions, threshold ):											#backward search

	literalDict = {}																			#heuristic frequency list
	levelCounter = 0
	nodeCounter = 0
	
	prelevantActionsList = {}																	#list of relevant actions for goal conditions

	allGoals = []																				#for loop checking
	temp = list( goalSet )																		#copy
	allGoals.append( temp )
	temp = []																					#no longer references allGoals copy
	
	pathDict = {}
	pathDict[ 0 ] = [ 0, '' ]																	#dictionary element: [ parent, 'action' ]
	
	node = [ 0 ]																				#node id
	temp = list( goalSet )
	node.append( temp )																			#goal set
	temp = list( allGoals )
	node.append( allGoals )																		#allGoals
	temp = []
	
	temp = list( node )
	queue = []
	queue.append( temp )
	temp = []
	node = []
	
	lastNodeID = 0
	nextNodeID = 0
	
	while len( queue ) > 0 and nodeCounter < threshold:
		parentNode = queue[ 0 ]
		queue = queue[ 1: len( queue ) ]														#dequeue
		
		if parentNode[ 0 ] > lastNodeID:
			lastNodeID = nextNodeID
			levelCounter = levelCounter + 1
		
		result = checkGoal( initialState, parentNode[ 1 ] )										#goal checking
		if result:
			return [ getPlan( parentNode, pathDict ), literalDict ]
		
		relevantActions = getRelevantActions( parentNode[ 1 ], actions )
		
		if relevantActions == []:																#indicates that current goal set is not reachable from the initial state and that no relevant actions exist to pursue further
			for goal in parentNode[ 1 ]:
				if goal in literalDict:															#updating heuristic frequencies
					literalDict[ goal ] = literalDict[ goal ] + 1
				else:
					literalDict[ goal ] = 1
		else:
			for action in relevantActions:
				nextGoalSet = applyAction( action, parentNode[ 1 ], parentNode[ 2 ], actions )
			
				if nextGoalSet != []:															#indicates that the action did not lead to looping and is hence valid
					nodeCounter = nodeCounter + 1
					
					if parentNode[ 0 ] == lastNodeID:
						nextNodeID = nodeCounter
					
					node = [ nodeCounter ]														#building new node
					temp1 = list( nextGoalSet )
					node.append( temp1 )
					temp1 = []
					temp1 = list( nextGoalSet )
					temp2 = list( parentNode[ 2 ] )
					temp2.append( temp1 )
					node.append( temp2 )
					temp1 = []
					temp2 = []
										
					temp1 = list( node )
					queue.append( temp1 )														#adding new node to queue
					temp1 = []
					node = []
				
					pathDict[ nodeCounter ] = [ parentNode[ 0 ], action ]
				else:
					for goal in parentNode[ 1 ]:												#updating heuristic frequencies
						if goal in literalDict:
							literalDict[ goal ] = literalDict[ goal ] + 1
						else:
							literalDict[ goal ] = 1
	return ( [ [], literalDict ] )