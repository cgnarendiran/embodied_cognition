
"use strict";

let DigitalInputEvent = require('./DigitalInputEvent.js');
let ExternalPower = require('./ExternalPower.js');
let RobotStateEvent = require('./RobotStateEvent.js');
let MotorPower = require('./MotorPower.js');
let BumperEvent = require('./BumperEvent.js');
let Sound = require('./Sound.js');
let ScanAngle = require('./ScanAngle.js');
let WheelDropEvent = require('./WheelDropEvent.js');
let ButtonEvent = require('./ButtonEvent.js');
let DigitalOutput = require('./DigitalOutput.js');
let KeyboardInput = require('./KeyboardInput.js');
let Led = require('./Led.js');
let DockInfraRed = require('./DockInfraRed.js');
let ControllerInfo = require('./ControllerInfo.js');
let SensorState = require('./SensorState.js');
let CliffEvent = require('./CliffEvent.js');
let VersionInfo = require('./VersionInfo.js');
let PowerSystemEvent = require('./PowerSystemEvent.js');
let AutoDockingFeedback = require('./AutoDockingFeedback.js');
let AutoDockingResult = require('./AutoDockingResult.js');
let AutoDockingGoal = require('./AutoDockingGoal.js');
let AutoDockingActionFeedback = require('./AutoDockingActionFeedback.js');
let AutoDockingAction = require('./AutoDockingAction.js');
let AutoDockingActionGoal = require('./AutoDockingActionGoal.js');
let AutoDockingActionResult = require('./AutoDockingActionResult.js');

module.exports = {
  DigitalInputEvent: DigitalInputEvent,
  ExternalPower: ExternalPower,
  RobotStateEvent: RobotStateEvent,
  MotorPower: MotorPower,
  BumperEvent: BumperEvent,
  Sound: Sound,
  ScanAngle: ScanAngle,
  WheelDropEvent: WheelDropEvent,
  ButtonEvent: ButtonEvent,
  DigitalOutput: DigitalOutput,
  KeyboardInput: KeyboardInput,
  Led: Led,
  DockInfraRed: DockInfraRed,
  ControllerInfo: ControllerInfo,
  SensorState: SensorState,
  CliffEvent: CliffEvent,
  VersionInfo: VersionInfo,
  PowerSystemEvent: PowerSystemEvent,
  AutoDockingFeedback: AutoDockingFeedback,
  AutoDockingResult: AutoDockingResult,
  AutoDockingGoal: AutoDockingGoal,
  AutoDockingActionFeedback: AutoDockingActionFeedback,
  AutoDockingAction: AutoDockingAction,
  AutoDockingActionGoal: AutoDockingActionGoal,
  AutoDockingActionResult: AutoDockingActionResult,
};
