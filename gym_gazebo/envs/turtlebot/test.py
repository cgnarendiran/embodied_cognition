#!/usr/bin/python

import rospy
# from gazebo_msgs.srv import DeleteModel, SpawnModel
# from gazebo_msgs.srv import GetModelState

# from geometry_msgs.msg import Pose
# from geometry_msgs.msg import Point, Quaternion
# import time
from geometry_msgs.msg import Twist
import numpy as np
# import transform_utils
# import PyKDL
from nav_msgs.msg import Odometry
import eigen as e
PI = 3.1415926535897

# rospy.wait_for_service("/gazebo/delete_model")
# rospy.wait_for_service("/gazebo/spawn_sdf_model")
# spawn_model = rospy.ServiceProxy("/gazebo/spawn_gazebo_model", SpawnModel)
# delete_model = rospy.ServiceProxy("/gazebo/delete_model", DeleteModel)


# # up_door_pose  =   Pose(Point(x=4.0, y=5.0,  z=0),   Quaternion(x=0,y=0,z=0,w=1) )

# # with open("/home/cgnarendiran/.gazebo/models/drc_practice_door_4x8/model.sdf", "r") as f:
# # 	product_xml = f.read()

# # # spawn_model('up_door', product_xml, '', up_door_pose, 'world')
# # delete_model('up_door')



# # up_door_pose  =   Pose(Point(x=4.0, y=5.0,  z=0),   Quaternion(x=0,y=0,z=0,w=1) )

# with open("/home/cgnarendiran/softwares/gym-gazebo/gym_gazebo/envs/assets/worlds/cube.urdf", "r") as f:
# 	product_xml = f.read()

# # spawn_model('light', product_xml, '',Pose(Point(x='', y='',  z=''),   Quaternion(x='',y='',z='',w='') ), 'world')
# # spawn_model('light', product_xml, '', Pose(Point(x=0.0, y=0.0,  z=1.0),   Quaternion(x=0,y=0,z=0,w=1) ), 'world')

# spawn_model('cube', product_xml, '', Pose(Point(x=0.01, y=0.0,  z=0.43),   Quaternion(x=0,y=0,z=0,w=1) ), 'mobile_base::base_footprint')

# rospy.init_node('spin', anonymous=True)
vel_pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=5)
# model_cords = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)

# objects_dict = {'cube1':['unit_box', 'link'], 'cube2': ['unit_box_0', 'link'], 'turtlebot':['mobile_base','']}
# pos = model_cords(objects_dict['turtlebot'][0], objects_dict['turtlebot'][1]).pose.orientation

# now = rospy.get_rostime().to_sec()
# vel_cmd = Twist()
# vel_cmd.linear.x = 0.0
# vel_cmd.angular.z = PI/8


# current_angle = 0
# t0 = rospy.Time.now().to_sec()
# while rospy.get_rostime().to_sec()< t0+4.0:
#     vel_pub.publish(vel_cmd)
#     t1 = rospy.Time.now().to_sec()
#     current_angle = vel_cmd.angular.z*(t1-t0)

# # current_angle = 0
# # t0 = rospy.Time.now().to_sec()
# # while(current_angle < PI/2):
# #     vel_pub.publish(vel_cmd)
# #     t1 = rospy.Time.now().to_sec()
# #     current_angle = vel_cmd.angular.z*(t1-t0)

# print(current_angle)
# # print(round(0.5,1))
# # print(pos)











# # Initialize the tf listener
# tf_listener = tf.TransformListener()

# # Set the odom frame
# odom_frame = '/odom'

# base_frame = '/base_footprint'



# # Get the current position
# (position, rotation) = get_odom(odom_frame, base_frame)


# Stop the robot before rotating
vel_cmd = Twist()

# Set the movement command to a rotation
vel_cmd.angular.z = PI/8

# Track the last angle measured
last_angle = rotation

# Track how far we have turned
turn_angle = 0

angular_tolerance = 0.0001
goal_angle = PI/2

# Begin the rotation
while abs(turn_angle + angular_tolerance) < abs(goal_angle) and not rospy.is_shutdown():
    # Publish the Twist message and sleep 1 cycle         
    vel_pub.publish(vel_cmd)
    
    r.sleep()
    
    # Get the current rotation
    (position, rotation) = get_odom()
    
    # Compute the amount of rotation since the last lopp
    delta_angle = normalize_angle(rotation - last_angle)
    
    turn_angle += delta_angle
    last_angle = rotation


#  def get_odom(odom_frame, base_frame):
#         # Get the current transform between the odom and base frames
#         try:
#             (trans, rot)  = tf_listener.lookupTransform(odom_frame, base_frame, rospy.Time(0))
#         except (tf.Exception, tf.ConnectivityException, tf.LookupException):
#             rospy.loginfo("TF Exception")
#             return



counter = 0

def Position(odom_data):

    global counter
    rospy.sleep(1)
    curr_time = odom_data.header.stamp
    pose = odom_data.pose.pose #  the x,y,z pose and quaternion orientation
    counter= counter+1
    print (counter, curr_time)
    print()
    quat = pose.orientation
    print (quat)
    q = e.Quaterniond(e.Vector4d(quat.x, quat.y, quat.z, quat.w)) # Eigen::Quaterniond(Eigen::Vector4d) (x, y, z, w)
    rpy = q.toRotationMatrix().eulerAngles(0, 1, 2);
    yaw = np.array(rpy)[2]
    print(yaw)

while not rospy.is_shutdown():
    rospy.init_node('oodometry', anonymous=True) #make node 
    rospy.Subscriber('/odom',Odometry,Position)

