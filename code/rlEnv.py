#This code is used to execute the RL algorithms

import datetime
import random
from operator import add
import tsg
import imrl

operators = []

def getStateID( representationDict, state ):
	id = [ 0 ] * len( representationDict )
	for condition in state:
		id[ representationDict[ condition ] ] = 1
	combined = ''.join( str( i ) for i in id )
	return int( combined, 2 )

def checkGoal( state, goals, atGoal ):
	result = True
	for condition in goals:
		if condition not in state:
			result = False
			break
			
	if result:
		if atGoal:
			return [ 0, True ]
		else:
			return[ 1, True ]
	else:
		return[ 0, False ]

def applyAction( action, state, goals, allActions, representationDict, actionProbabilityThreshold, atGoal ):
	global operators
	
	result = 0																								#indicates successful action execution
	nextState = list( state )
	if random.random() > actionProbabilityThreshold:
		action = random.randint( 0, len( allActions ) - 1 )													#random selection of action
		result = 1
	
	if result == 0:
		for condition in allActions[ action ][ 1 ]:															#checking preconditions
			if condition not in state:
				result = 2																					#indicates precondition failure
				break
		
		if result == 0:
			for condition in allActions[ action ][ 2 ]:
				nextState.append( condition )
			for condition in allActions[ action ][ 3 ]:
				if condition in nextState:
					nextState.remove( condition )
#			"execute allActions[ action ][ 0 ]"
	
	[ reward, goalIndicator ] = checkGoal( nextState, goals, atGoal )
	
	tsg.tsg( state, allActions[ action ][ 0 ], nextState, representationDict )
		
	exitFlag = True
	for goal in goals:
		goalFlag = False
		relevantAtoms = []
		distinctActions = []
		for atom in tsg.conditionalEffectFormula:
			atomList = atom.split( ';' )
			if goal in atomList[ 1 ]:
				if tsg.pcPos[ atom ] + tsg.pcNeg[ atom ] > tsg.minExamples and tsg.pcPos[ atom ]/( tsg.pcPos[ atom ] + tsg.pcNeg[ atom ] ) > tsg.minProbability:
					relevantAtoms.append( atomList )
					if atomList[ 0 ] not in distinctActions:
						distinctActions.append( atomList[ 0 ] )
					goalFlag = True	
		if not goalFlag:
			exitFlag = False																				#have not yet found a suitable operator; need to search more
			break
		else:																								#operator found, build in format usable by planner
			for dAction in distinctActions:
				operator = [ dAction ]
				preConditions = []
				for atom in relevantAtoms:
					if atom[ 0 ] == dAction:
						preConditions.append( atom[ 2 ] )
				operator.append( preConditions )
				operator.append( [ goal ] )
				operator.append( [] )
				
				for atom in tsg.conditionalEffectFormula:
					atomList = atom.split( ';' )
					if dAction == atomList[ 0 ]:
						if atomList[ 1 ][ 0 ] == '~':
							if goal.split( ',' )[ 0 ] != atomList[ 1 ].split( ',' )[ 0 ][ 1: ]:
								for preC in operator[ 1 ]:
									if preC == atomList[ 2 ]:
										if atomList[ 1 ][ 1: ] not in operator[ 3 ]:
											operator[ 3 ].append( atomList[ 1 ][ 1: ] )
						else:
							if goal.split( ',' )[ 0 ] != atomList[ 1 ].split( ',' )[ 0 ]:
								for preC in operator[ 1 ]:
									if preC == atomList[ 2 ]:
										if atomList[ 1 ] not in operator[ 2 ]:
											operator[ 2 ].append( atomList[ 1 ] )
		
			operators.append( operator )	
	return [ reward, goalIndicator, nextState, exitFlag, result ]
	
def isSalientEvent( state, salientEvent, salientEventIndex ):
	result = 1
	for condition in salientEvent:
		if condition not in state:
			result = 0
			break
	
	if result == 0 and salientEventIndex == 0:
		return[ 0, False ]
	
	if result == 1 and salientEventIndex == 1:
		return[ 1, False ]	
	
	if result == 0 and salientEventIndex > 0:
		return[ 2, True ]
		
	if result == 1 and salientEventIndex != 1:
		return[ 1, True ]

def executeIMRLExp( initialState, goals, salientEvent, allActions, representationDict, actionProbabilityThreshold, expIterations = 1, alpha = 0.1, gamma = 0.9, epsilon = 0.1, tau = 0.5, iterations = 100000, outputFile = 'op/imrlOutput' ):
	global operators
	
	startTime = datetime.datetime.now()
	
	opFileName = outputFile + 'Exp' + str ( startTime ).replace( ':', '-' ) + str( '.txt' )
	opFile = open( opFileName, 'w' )
	opFile.write( 'startTime: ' + str( startTime ) + '\nAlpha: ' + str( alpha ) + '\nGamma: ' + str ( gamma ) + '\nEpsilon: ' + str( epsilon ) + '\nTau: ' + str( tau ) + '\nNumber of actions: ' + str( len( allActions ) ) + '\nNumber of options: ' + str( 2 ) + '\nIterations: ' + str( iterations ) + '\nExpIterations: ' + str( expIterations ) + '\n\n' )
	
	actionCounter = [] 
	intrinsicRewardCounter = []
	extrinsicRewardCounter = []
	
	for ctr in range( expIterations ):
		[ ac, erc ] = imrl.imrl( representationDict, initialState, goals, salientEvent, allActions, actionProbabilityThreshold, fDict2, alpha, gamma, epsilon, tau, iterations, outputFile )
		tsg.plainEffectFormula = {}
		tsg.conditionalEffectFormula = {}
		tsg.pcPos = {}
		tsg.pcNeg = {}
		
		actionCounter = list( map( add, actionCounter, ac ) )
		extrinsicRewardCounter = list( map( add, extrinsicRewardCounter, erc ) )
	
	opFile.write( '\nactionCounter:\n' + str( actionCounter ) + '\n\nextrinsicRewardCounter:\n' + str( extrinsicRewardCounter ) )	
	endTime = datetime.datetime.now()
	opFile.write( '\n\n\nendTime: ' + str( endTime ) + '\ntimeTaken: ' + str( endTime - startTime ) )
	opFile.close()
	
	result = False
	selected = list( operators )
	for operator in operators:
		for goal in goals:
			if goal in operator[ 2 ]:
				result = True
				break
	
	return[ result, selected ]
	
fDict2 = { 'getStateID': getStateID, 'applyAction': applyAction, 'isSalientEvent': isSalientEvent }